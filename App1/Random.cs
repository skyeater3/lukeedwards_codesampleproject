using System;

namespace App1
{
	public static class RandomValue
	{
		private static Random rGen;

		static RandomValue()
		{
			rGen = new Random();
		}

		public static int GetRandomInt(int maxValue)
		{
			return GetRandomInt(0, maxValue);
		}

		public static int GetRandomInt(int minValue, int maxValue)
		{
			return rGen.Next(minValue, maxValue);
		}
	}
}