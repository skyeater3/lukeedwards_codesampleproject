using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App1
{
	public class Collision
	{
		// if we have penetrated, this is the minimum amount we need to move the penetrator, to stop penetration
		public Vector2f penetrationCorrection;
		// normalised time 0->1 at which collision occurs. For penetration, this is 0
		public float collisionTime;

		// normal of point we collided with 
		public Vector2f collisionNormal;
		// Note -- if the dotProduct of the velocity with the collisionNormal is -ve, reflect. If +ve, push.
		//
		// if velocity is along line \ (moving down) and there is a collision ([1]ball moves into paddle, [2]paddle moves into ball)
		//
		//[1]        [2]=========	
		//  \  |			\
		//	 \ |			 \
		//    \|			  \
		//  ========    
		//
		// in [1] we want to reflect the direction of the ball to simulate bounce.
		// in [2] we want to accelerate the velocity of the ball to simulate bounce (in the same direction).

		// also adding in collisionLocation
		private Vector2f collisionLocation;
		public Vector2f CollisionLocation { get { return penetrationCorrection != null ? collisionLocation + penetrationCorrection : collisionLocation; } }

		public IPhysicsCapableObject collisionWith { get; set; }	

		public Collision(float collisionTime, Vector2f collisionNormal, Vector2f collisionLocation, Vector2f penetrationCorrection = null)
		{
			this.collisionTime = collisionTime;
			this.collisionNormal = collisionNormal;
			this.penetrationCorrection = penetrationCorrection;
			this.collisionLocation = collisionLocation;
		}

		private void ResolveVelocity(ref Vector2f velocity)
		{
			float velocityMagnitude = velocity.Magnitude;

			if (velocity.dot(collisionNormal) <= 0)
			{
				velocity = Vector2f.Reflect(velocity, collisionNormal);
			}
			else
			{
				// this is cheap, but meh
				// edit: and now I'm not actually sure it works. I think eject is pushing things too far. Meaning it doesn't collide properly
				// and sort of skips a frame. I thought this was to do with the unintentional double update in the physicsUpdate loop, but now I think i've just missed something
				// anyway. Deadline. don't have time to fix it right now. But it's close. I'll find it after I submit.
				velocity *= 1.1f;
			}

			if (velocity.Magnitude > velocityMagnitude * 1.25f)
				velocity = velocity.Normalise() * velocityMagnitude * 1.25f;
		}

		public void ResolvePositionAndVelocity(ref Vector2f position, ref Vector2f velocity, double dt)
		{
			if (penetrationCorrection != null)
				position += penetrationCorrection;

			position = position + velocity * collisionTime * (float) dt;

			ResolveVelocity(ref velocity);

			position = position + velocity * (1.0f - collisionTime) * (float) dt;
		}
	}

	public static class PhysicsSphereRectCollision
	{
		// http://www.gamasutra.com/view/feature/131790/simple_intersection_tests_for_games.php?page=2

		//	http://stackoverflow.com/questions/18704999/how-to-fix-circle-and-rectangle-overlap-in-collision-response/18790389#18790389


			
		public static Collision CircleRectCollisionHandler(IPhysicsCapableObject rect, IPhysicsCapableObject circle, float dt)
		{
			Vector2f start, end;
			Rect r = Rect.CreateFromPCO(rect);

			start = circle.Position;
			end = circle.Position + circle.Velocity * dt;

			// start += rect.Velocity; //derrrr --- this was stupid
			end += rect.Velocity * dt;
			
			return DetectSphereRectCollision(r, start, end, circle.Size.x);
		}

		private static Collision EjectFromRect(Rect rect, Vector2f start, Vector2f end, float radius)
		{
			float L = rect.left;
			float T = rect.top;
			float R = rect.right;
			float B = rect.bottom;
			
			Vector2f penetrationCorrection = new Vector2f(0, 0);

			Vector2f w = start - rect.centre;
			if (w.x > 0)    // largely irrelevant if 0.
			{
				penetrationCorrection.x = R - start.x + radius;
			}
			else
			{
				penetrationCorrection.x = L - start.x - radius;
			}

			if (w.y < 0)    // for y, default to kicking it out of the top if half way in (w.y = 0)
			{
				penetrationCorrection.y = B - start.y - radius;
			}
			else
			{
				penetrationCorrection.y = T - start.y + radius;
			}

			// finally decide where the shortest distance to eject the sphere is
			// in the case where the ball is between the paddle and the wall, this might happen a few times.
			// where it kicks it out horizontally, then resets the location... back inside the rect.
			// eventually it should settle down (I hope)

			//Vector2f percentP = new Vector2f(penetration.x / rect.width, penetration.y / rect.height);
			bool notCornerEjection = true;
			// if the shortest ejection distance might be at the corner...
			if (Math.Abs(penetrationCorrection.x) < radius && Math.Abs(penetrationCorrection.y) < radius)
			{
				var pcMag = penetrationCorrection.Magnitude;
				if (pcMag < radius)
				{
					if (penetrationCorrection.x > pcMag && penetrationCorrection.y > pcMag)
					{
						// corner ejection
						notCornerEjection = false;
					}
				}
			}

			if (notCornerEjection)
			{
				if (penetrationCorrection.x > penetrationCorrection.y)
				{
					penetrationCorrection.x = 0;
				}
				else
				{
					penetrationCorrection.y = 0;
				}
			}

			// the normal returned here for corner ejections is wrong... but we are too close to get it correctly?
			// I guess we apply the correction first... ? Sod it.
			// this isn't an exercise in perfect physics.

			return new Collision(0, penetrationCorrection.Normalise(), start, penetrationCorrection);
		}

		/// <summary>
		/// Accuratley makes sure the position is inside the rect, and not just very close to one of its corners
		/// </summary>
		/// <param name="rect">Rect to check</param>
		/// <param name="start">position of circle</param>
		/// <param name="radius">radius of circle</param>
		/// <returns>True if inside, false otherwise</returns>
		private static bool InsideRectAccurate(Rect rect, Vector2f start, float radius)
		{
			float s, d = 0;
			if (start.x < rect.left)
			{
				s = start.x - rect.left;
				d += s * s;
			}
			else if (start.x > rect.right)
			{
				s = start.x - rect.right;
				d += s * s;
			}

			if (start.y < rect.bottom)
			{
				s = start.y - rect.bottom;
				d += s * s;
			}
			else if (start.y > rect.top)
			{
				s = start.y - rect.top;
				d += s * s;
			}

			return d <= radius * radius;
		}

		/// <summary>
		/// Detects and returns any collision that occurs
		/// </summary>
		/// <param name="rect">bounding rect of object to test for collision</param>
		/// <param name="start">Start position of sphere</param>
		/// <param name="end">End position of sphere. Note that this is relative to the rect we are testing against (if both are moving, this is in the reference frame of the rect, as if it were stationary)</param>
		/// <param name="radius">radius of sphere</param>
		/// <returns>Any collision, or null</returns>
		private static Collision DetectSphereRectCollision(Rect rect, Vector2f start, Vector2f end, float radius)
		{
			float L = rect.left;
			float T = rect.top;
			float R = rect.right;
			float B = rect.bottom;

			float maxX = Math.Max(start.x, end.x);
			float minX = Math.Min(start.x, end.x);
			float maxY = Math.Max(start.y, end.y);
			float minY = Math.Min(start.y, end.y);

			// earliest exit possible
			if ((maxX + radius < L) ||
				(minX - radius > R) ||
				(minY - radius > T) ||
				(maxY + radius < B))
			{
				return null;
			}

			// if we start inside the rect, then figure out how far in we are.
			// and exit immediately
			if (InsideRectAccurate(rect, start, radius))
			{
				return EjectFromRect(rect, start, end, radius);
			}

			Vector2f d = end - start;
			Vector2f invd = new Vector2f(1 / d.x, 1 / d.y);

			float ltime = float.MaxValue, rtime = float.MaxValue, ttime = float.MaxValue, btime = float.MaxValue;
			bool checkLeft = false, checkRight = false, checkTop = false, checkBottom = false;

			if (minX < L)
			{
				// at some point, the sphere was to the left of box
				// check if only here
				if (start.x - radius < L && end.x + radius > L)
				{
					ltime = ((L - radius) - start.x) * invd.x;
					if (ltime >= 0.0f && ltime <= 1.0f)
					{
						float ly = d.y * ltime + start.y;
						if (ly <= T && ly >= B)
						{
							return new Collision(ltime, new Vector2f(-1, 0), new Vector2f(L - radius, ly));
						}
					}

					checkLeft = true;
				}
			}
			if (maxX > R)
			{
				// at some point to right of box
				// check if only here
				if (start.x + radius > R && end.x - radius < R)
				{
					rtime = (start.x - (R + radius)) * -invd.x;
					if (rtime >= 0.0f && rtime <= 1.0f)
					{
						float ry = d.y * rtime + start.y;
						if (ry <= T && ry >= B)
						{
							return new Collision(rtime, new Vector2f(1, 0), new Vector2f(R + radius, ry));
						}
					}

					checkRight = true;
				}
			}
			if (minY < B)
			{
				// at some point, to bottom of box
				// check if only here
				if (start.y - radius < B && end.y + radius > B)
				{
					btime = ((B - radius) - start.y) * invd.y;
					if (btime >= 0.0f && btime <= 1.0f)
					{
						float bx = d.x * btime + start.x;
						if (bx >= L && bx <= R)
						{
							return new Collision(btime, new Vector2f(0, -1), new Vector2f(bx, B - radius));
						}
					}

					checkBottom = true;
				}
			}
			if (maxY > T)
			{
				// at some point to top of box
				// check if only here
				if (start.y + radius > T && end.y - radius < T)
				{
					ttime = (start.y - (T + radius)) * -invd.y;
					if (ttime >= 0.0f && ttime <= 1.0f)
					{
						float tx = d.x * ttime + start.x;
						// Does the collisions point lie on the top side?
						if (tx >= L && tx <= R)
						{
							return new Collision(ttime, new Vector2f(0, 1), new Vector2f(tx, T + radius));
						}
					}

					checkTop = true;
				}
			}

			/* 
			   if we get here, we have either hit a corner or just narrowly passed by a corner
			   (or not actually hit a corner yet!)
			 
			   we can say that collision position is definitely in one of these 4 sectors
			    because if it was in the centre sectors, the collision (if it exists) would have returned above
				
				the other possibility is that we are just not quite close enough to a corner to hit it yet.		 
			
			 L&T	|       |  R&T 
			        |       |
				  --|-------|--
			    	|#######|   
					|#######|
			        |#######|   
			      --|-------|--
					|		| 
			 L&B	|       |  R&B

			*/

			// if any of these times is < 0 then we are approaching a corner, but may not hit it just yet
			// we are already across the threshold for one of the sides (at least) and may need to account for the other corner

			// It occurs to me that for very fast moving objects that isn't necessarily the case
			// and we might end up with the possibilty of three checkable positions --
			// for a fast moving and very close to surface object, which doesn't impact until after it crosses
			// the 'other' side from the wrong direction
			if (checkBottom ^ checkLeft ^ checkRight ^ checkTop)
			{
				// if we hit anything this corner will be one that we don't approach from the correct side
				// so if we are moving down, it will be the bottom corner
				// or up the top.
				// to the left, the right corner,
				// or to the right, the left
				if (!(checkTop || checkBottom))
				{
					if (d.y > 0)
						checkTop = true;
					else
						checkBottom = true;
				}
				if (!(checkLeft || checkRight))
				{
					if (d.x > 0)
						checkRight = true;
					else
						checkLeft = true;
				}

			}

			// we still allow for the possiblity that there is no collision
			Vector2f corner = new Vector2f(checkLeft ? L : R, checkTop ? T : B);
			Vector2f sToC = corner - start;
			Vector2f eToC = corner - end;
			float sqrDistSToC = sToC.dot(sToC);
			float sqrDistEToC = eToC.dot(eToC);
			float radiusSqr = radius * radius;
			float time = float.MaxValue;

			// definitely no collision
			if (sqrDistSToC > radiusSqr && sqrDistEToC > radiusSqr)
				return null;

			if (sqrDistSToC > radiusSqr && sqrDistEToC < radiusSqr)
			{
				double inverseRadius = 1.0 / radius;
				double lineLength = d.Magnitude;
				double cornerDistance = sToC.Magnitude;
				double innerAngle = Math.Acos((sToC.x * d.x + sToC.y * d.y) / (lineLength * cornerDistance));
				Vector2f normal;

				if (innerAngle == 0.0f)
				{
					time = (float) ((cornerDistance - radius) / lineLength);

					// If time is outside the boundaries, return null. This algorithm can
					// return a negative time which indicates a previous intersection, and
					// can also return a time > 1.0f which can predict a corner intersection.
					if (time > 1.0f || time < 0.0f)
					{
						return null;
					}

					normal = sToC / (float) cornerDistance;
					return new Collision(time, normal, start + d * time);
					//return new Intersection(intersectionX, intersectionY, timeOfCollision, normalX, normalY, cornerX, cornerY);
				}

				double innerAngleSin = Math.Sin(innerAngle);
				double angle1Sin = innerAngleSin * cornerDistance * inverseRadius;

				// The angle is too large, there cannot be an intersection
				if (Math.Abs(angle1Sin) > 1.0f)
				{
					return null;
				}

				double angle1 = Math.PI - Math.Asin(angle1Sin);
				double angle2 = Math.PI - innerAngle - angle1;
				double intersectionDistance = radius * Math.Sin(angle2) / innerAngleSin;

				// Solve for time
				time = (float) (intersectionDistance / lineLength);

				// If time is outside the boundaries, return null. This algorithm can
				// return a negative time which indicates a previous intersection, and
				// can also return a time > 1.0f which can predict a corner intersection.
				if (time > 1.0f || time < 0.0f)
				{
					return null;
				}

				// Solve the intersection and normal
				

				time = d.dot(sToC) / d.Magnitude;

				Vector2f centreOfBallAtCollisionTime = start + d * time;
				// // // normal = new Vector2f(checkLeft ? -1 : 1, checkTop ? 1 : -1);
				normal = new Vector2f((centreOfBallAtCollisionTime.x - (checkLeft ? L : R)) * (float) inverseRadius,
									  (centreOfBallAtCollisionTime.y - (checkTop ? T : B)) * (float) inverseRadius);

				//
				//float ix = time * dx + start.x;
				//float iy = time * dy + start.y;
				//float nx = (float) ((ix - cornerX) * inverseRadius);
				//float ny = (float) ((iy - cornerY) * inverseRadius);
				//

				return new Collision(time, normal, centreOfBallAtCollisionTime);
			}

			// sanity check
			if (sqrDistSToC < radiusSqr)
			{
				Console.WriteLine("oops!?");
				return EjectFromRect(rect, start, end, radius);
			}

			// I can't see how we could reasonably get to this point
			Console.WriteLine("What's going on here then?");
			return null;
		}
	}
}