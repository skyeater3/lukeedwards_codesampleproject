namespace App1
{
	public static class DeltaTime
	{
		private static long startTime;
		private static long currentTime;
		private static long lastTime;
		private static long deltaTime;

		public static double GetDeltaTime()
		{
			currentTime = Java.Lang.JavaSystem.NanoTime();
			deltaTime = currentTime - lastTime;
			lastTime = currentTime;
			return deltaTime / 1000000;
		}

		static DeltaTime()
		{
			startTime = Java.Lang.JavaSystem.NanoTime();
			lastTime = startTime;
		}
	}
}