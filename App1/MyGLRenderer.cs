﻿using Android.Opengl;
using Javax.Microedition.Khronos.Opengles;

namespace App1
{
	public class MyGLRenderer : Java.Lang.Object, GLSurfaceView.IRenderer
	{
		public void OnDrawFrame(IGL10 gl)
		{
			// Draw background color
			GLES11.GlClear(GL11.GlColorBufferBit | GL11.GlDepthBufferBit);

			// Set GL_MODELVIEW transformation mode
			GLES11.GlMatrixMode(GL11.GlModelview);
			GLES11.GlLoadIdentity();   // reset the matrix to its default state

			// When using GL_MODELVIEW, you must set the view point
			GLU.GluLookAt(gl, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

			GameStateLoop.GameStateLoopInstance.RunStateLoopUpdate();
		}

		public void OnSurfaceChanged(IGL10 gl, int width, int height)
		{
			// Adjust the viewport based on geometry changes
			// such as screen rotations
			GLES11.GlViewport(0, 0, width, height);

			// make adjustments for screen ratio
			float ratio = (float) width / height;
			GLES11.GlMatrixMode(GL11.GlProjection);        // set matrix to projection mode
			GLES11.GlLoadIdentity();                        // reset the matrix to its default state
			GLES11.GlFrustumf(-ratio, ratio, -1, 1, 3, 7);  // apply the projection matrix

			ScreenBoundaries.ChangeScreenGeometry(0, 0, width, height);
		}

		public void OnSurfaceCreated(IGL10 gl, Javax.Microedition.Khronos.Egl.EGLConfig config)
		{
			//Set the background frame color
			GLES11.GlClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		}
	}
}