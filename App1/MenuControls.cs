using Android.Content;
using Android.Text.Method;
using Android.Views;
using Android.Widget;
using System;

namespace App1
{
	public static class MenuControls
	{
		private static Android.Views.ViewGroup.LayoutParams layoutParams = new Android.Views.ViewGroup.LayoutParams(Android.Views.ViewGroup.LayoutParams.MatchParent, Android.Views.ViewGroup.LayoutParams.MatchParent);

		public static void AddMainMenuView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var layoutInflater = (LayoutInflater) OpenGLES10Activity.GetMainActivity.GetSystemService(Context.LayoutInflaterService);
				RelativeLayout rl = new RelativeLayout(OpenGLES10Activity.GetMainActivity.ApplicationContext);
				layoutInflater.Inflate(Resource.Layout.MainMenuLayout, rl);

				if (rl != null)
					OpenGLES10Activity.GetMainActivity.AddContentView(rl, layoutParams);
				else
					Console.WriteLine("Main Menu View Null");

				Button startGameButton = (Button) rl.FindViewById(Resource.Id.startGameButton);
				startGameButton.Click += StartGameButton_Click;

				Button aboutButton = (Button) rl.FindViewById(Resource.Id.aboutButton);
				aboutButton.Click += AboutButton_Click;

				return;
			});
		}

		private static void AboutButton_Click(object sender, EventArgs e)
		{
			GameStateLoop.GameStateLoopInstance.ChangeState(GameStateLoop.State.Menu_About);
		}

		private static void StartGameButton_Click(object sender, EventArgs e)
		{
			GameStateLoop.GameStateLoopInstance.ChangeState(GameStateLoop.State.Game_Loading);
		}

		public static void RemoveMainMenuView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var mainMenuView = OpenGLES10Activity.GetMainActivity.FindViewById(Resource.Id.mainMenuId);
				Android.Views.ViewGroup mainMenuViewGroup = (Android.Views.ViewGroup) mainMenuView?.Parent;
				mainMenuViewGroup?.RemoveView(mainMenuView);
			});
		}

		public static void AddAboutMenuView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var layoutInflater = (LayoutInflater) OpenGLES10Activity.GetMainActivity.GetSystemService(Context.LayoutInflaterService);
				RelativeLayout rl = new RelativeLayout(OpenGLES10Activity.GetMainActivity.ApplicationContext);
				layoutInflater.Inflate(Resource.Layout.AboutMenuLayout, rl);

				OpenGLES10Activity.GetMainActivity.AddContentView(rl, layoutParams);

				TextView aboutMeText = (TextView) rl.FindViewById(Resource.Id.aboutMeText);
				//aboutMeText.Context
				//aboutMeText.SetText(Android.Text.Html.FromHtml(aboutMeText.Text, OpenGLES10Activity.GetMainActivity, null).ToString());
				//aboutMeText.Text = Android.Text.Html.FromHtml(aboutMeText.Text, OpenGLES10Activity.GetMainActivity, null).ToString();
				//aboutMeText.SetText(Android.Text.Html.FromHtml(aboutMeText.Text, OpenGLES10Activity.GetMainActivity, null).ToString(), TextView.BufferType.Normal);
				aboutMeText.TextFormatted = Android.Text.Html.FromHtml(aboutMeText.Text, OpenGLES10Activity.GetMainActivity, new HtmlTagHandler());
				aboutMeText.LinksClickable = true;
				aboutMeText.MovementMethod = LinkMovementMethod.Instance;

				Button backToMainMenu = (Button) rl.FindViewById(Resource.Id.backFromAboutMenuButton);
				backToMainMenu.Click += BackToMainMenu_Click;
			});
		}

		internal static void AddHighscoresMenuView()
		{
			throw new NotImplementedException();
		}

		internal static void AddGameOverMenuView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var layoutInflater = (LayoutInflater) OpenGLES10Activity.GetMainActivity.GetSystemService(Context.LayoutInflaterService);
				RelativeLayout rl = new RelativeLayout(OpenGLES10Activity.GetMainActivity.ApplicationContext);
				layoutInflater.Inflate(Resource.Layout.GameOverLayout, rl);

				OpenGLES10Activity.GetMainActivity.AddContentView(rl, layoutParams);

				Button backToMainMenu = (Button) rl.FindViewById(Resource.Id.gameOverBackToMainMenu);
				backToMainMenu.Click += BackToMainMenu_Click;

				Button playAgain = (Button) rl.FindViewById(Resource.Id.playAgain);
				playAgain.Click += StartGameButton_Click;

				TextView scoreText = (TextView) rl.FindViewById(Resource.Id.scoreTotal);
				GameStateLoop.GameStateLoopInstance.SetScoreTextView(scoreText);
			});
		}

		public static void RemoveAboutMenuView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var mainMenuView = OpenGLES10Activity.GetMainActivity.FindViewById(Resource.Id.aboutMeLayout);
				Android.Views.ViewGroup mainMenuViewGroup = (Android.Views.ViewGroup) mainMenuView?.Parent;
				mainMenuViewGroup?.RemoveView(mainMenuView);
			});
		}

		internal static void RemoveGameOverMenuView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var mainMenuView = OpenGLES10Activity.GetMainActivity.FindViewById(Resource.Id.gameOverLayout);
				Android.Views.ViewGroup mainMenuViewGroup = (Android.Views.ViewGroup) mainMenuView?.Parent;
				mainMenuViewGroup?.RemoveView(mainMenuView);
			});
		}

		internal static void RemoveHighscoresView()
		{
			throw new NotImplementedException();
		}

		public static void AddInGameUIElements()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var layoutInflater = (LayoutInflater) OpenGLES10Activity.GetMainActivity.GetSystemService(Context.LayoutInflaterService);
				RelativeLayout rl = new RelativeLayout(OpenGLES10Activity.GetMainActivity.ApplicationContext);
				layoutInflater.Inflate(Resource.Layout.InGameUI, rl);

				OpenGLES10Activity.GetMainActivity.AddContentView(rl, layoutParams);

				ImageView pause = (ImageView) rl.FindViewById(Resource.Id.pauseButton);
				pause.Click += Pause_Click;

				TextView scoreText = (TextView) rl.FindViewById(Resource.Id.scoreTotal);
				// scoreText.SetText(GameStateLoop.ScoreText, TextView.BufferType.Normal);

				GameStateLoop.GameStateLoopInstance.SetScoreTextView(scoreText);
			});
		}

		internal static void AddPauseMenuView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var layoutInflater = (LayoutInflater) OpenGLES10Activity.GetMainActivity.GetSystemService(Context.LayoutInflaterService);
				RelativeLayout rl = new RelativeLayout(OpenGLES10Activity.GetMainActivity.ApplicationContext);
				layoutInflater.Inflate(Resource.Layout.GamePausedLayout, rl);

				OpenGLES10Activity.GetMainActivity.AddContentView(rl, layoutParams);

				Button resumeGameButton = (Button) rl.FindViewById(Resource.Id.resumeGameButton);
				resumeGameButton.Click += ResumeGameButton_Click;

				Button restartGameButton = (Button) rl.FindViewById(Resource.Id.restartGameButton);
				restartGameButton.Click += RestartGameButton_Click;

				Button backToMainMenu = (Button) rl.FindViewById(Resource.Id.mainMenuButton);
				backToMainMenu.Click += BackToMainMenu_Click;

				TextView scoreText = (TextView) rl.FindViewById(Resource.Id.scoreTotal);
				GameStateLoop.GameStateLoopInstance.SetScoreTextView(scoreText);
			});
		}

		private static void BackToMainMenu_Click(object sender, EventArgs e)
		{
			GameStateLoop.GameStateLoopInstance.ChangeState(GameStateLoop.State.Menu_Main);
		}

		private static void RestartGameButton_Click(object sender, EventArgs e)
		{
			GameStateLoop.GameStateLoopInstance.ChangeState(GameStateLoop.State.Game_Loading);
		}

		private static void ResumeGameButton_Click(object sender, EventArgs e)
		{
			GameStateLoop.GameStateLoopInstance.ChangeState(GameStateLoop.State.Game_Play);
		}

		private static void Pause_Click(object sender, EventArgs e)
		{
			GameStateLoop.GameStateLoopInstance.ChangeState(GameStateLoop.State.Game_Pause);
		}

		internal static void RemoveInGameUIElements()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var mainMenuView = OpenGLES10Activity.GetMainActivity.FindViewById(Resource.Id.inGameUI);
				Android.Views.ViewGroup mainMenuViewGroup = (Android.Views.ViewGroup) mainMenuView?.Parent;
				mainMenuViewGroup?.RemoveView(mainMenuView);
			});
		}

		internal static void RemovePauseMenuView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				var mainMenuView = OpenGLES10Activity.GetMainActivity.FindViewById(Resource.Id.pauseMenu);
				Android.Views.ViewGroup mainMenuViewGroup = (Android.Views.ViewGroup) mainMenuView?.Parent;
				mainMenuViewGroup?.RemoveView(mainMenuView);
			});
		}
	}
}