using System;
using System.Diagnostics;

namespace App1
{
	public class Rect
	{
		public float left, right, top, bottom;
		public float angle = 0;
		public float height, width;
		public Vector2f centre;

		[DebuggerStepThrough]
		public Rect(float left, float top, float width, float height)
		{
			this.left = left;
			this.right = left + width;
			this.width = width;
			this.top = top;
			this.bottom = top - height;
			this.height = height;
			centre = new Vector2f(left + width / 2f, top - height / 2f);
		}

		[DebuggerStepThrough]
		public static bool Intersect(Rect a, Rect b)
		{
			return (a.left <= b.right &&
					b.left <= a.right &&
					a.top >= b.bottom &&
					b.top >= a.bottom);
		}

		[DebuggerStepThrough]
		public static Rect Translate(Rect r, Vector2f t)
		{
			return new Rect(r.left + t.x, r.top + t.y, r.width, r.height);
		}

		public static float IntersectingArea(Rect a, Rect b, out float widthOfIntersect, out float heightOfIntersect)
		{
			widthOfIntersect = Math.Min(a.right, b.right) - Math.Max(a.left, b.left);
			heightOfIntersect = Math.Min(a.top, b.top) - Math.Max(a.bottom, b.bottom);

			return Math.Max(0, widthOfIntersect * heightOfIntersect);
		}

		[DebuggerStepThrough]
		public static Rect CreateFromPCO(IPhysicsCapableObject pco)
		{
			return new Rect(pco.Position.x - pco.Size.x / 2f,
							pco.Position.y + pco.Size.y / 2f,
							pco.Size.x,
							pco.Size.y);
		}
	}
}