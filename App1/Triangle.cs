﻿using Android.Opengl;
using Java.Nio;
using Javax.Microedition.Khronos.Opengles;

namespace App1
{
	/**
	 * A two-dimensional triangle for use as a drawn object in OpenGL ES 1.0/1.1.
	 */

	public class Triangle
	{
		private readonly FloatBuffer vertexBuffer;

		// number of coordinates per vertex in this array
		private static readonly int COORDS_PER_VERTEX = 3;

		private static float[] triangleCoords =
		{
			// in counterclockwise order:
			0.0f,  0.622008459f, 0.0f,// top
		   -0.5f, -0.311004243f, 0.0f,// bottom left
			0.5f, -0.311004243f, 0.0f // bottom right
		};

		private float[] color = { 0.63671875f, 0.76953125f, 0.22265625f, 0.0f };

		/**
		 * Sets up the drawing object data for use in an OpenGL ES context.
		 */

		public Triangle()
		{
			// initialize vertex byte buffer for shape coordinates
			ByteBuffer bb = ByteBuffer.AllocateDirect(
					// (number of coordinate values * 4 bytes per float)
					triangleCoords.Length * 4);
			// use the device hardware's native byte order
			bb.Order(ByteOrder.NativeOrder());

			// create a floating point buffer from the ByteBuffer
			vertexBuffer = bb.AsFloatBuffer();
			// add the coordinates to the FloatBuffer
			vertexBuffer.Put(triangleCoords);
			// set the buffer to read the first coordinate
			vertexBuffer.Position(0);
		}

		/**
		 * Encapsulates the OpenGL ES instructions for drawing this shape.
		 *
		 * @param gl - The OpenGL ES context in which to draw this shape.
		 */

		public void Draw(IGL10 gl)
		{
			// Since this shape uses vertex arrays, enable them
			GLES11.GlEnableClientState(GL10.GlVertexArray);

			// draw the shape
			GLES11.GlColor4f(       // set color:
					color[0], color[1],
					color[2], color[3]);
			GLES11.GlVertexPointer( // point to vertex data:
					COORDS_PER_VERTEX,
					GL10.GlFloat, 0, vertexBuffer);
			GLES11.GlDrawArrays(    // draw shape:
					GL10.GlTriangles, 0,
					triangleCoords.Length / COORDS_PER_VERTEX);

			// Disable vertex array drawing to avoid
			// conflicts with shapes that don't use it
			GLES11.GlDisableClientState(GL10.GlVertexArray);
		}
	}
}