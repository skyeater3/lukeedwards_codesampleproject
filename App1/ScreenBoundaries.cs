using Android.Opengl;

namespace App1
{
	public static class ScreenBoundaries
	{
		public static float LeftEdgeOfScreen { get; set; }
		public static float RightEdgeOfScreen { get; set; }
		public static float TopEdgeOfScreen { get; set; }
		public static float BottomEdgeOfScreen { get; set; }
		public static bool ScreenBoundariesSet { get; set; }

		static ScreenBoundaries()
		{
			ScreenBoundariesSet = false;

			LeftEdgeOfScreen = 0f;
			RightEdgeOfScreen = 0f;
			TopEdgeOfScreen = 0f;
			BottomEdgeOfScreen = 0f;
		}

		public static void ChangeScreenGeometry(int left, int top, int width, int height)
		{
			float[] matProjection = new float[16];
			float[] matModelView = new float[16];
			int[] view = new int[4];
			//view[0] = left; view[1] = top; view[2] = width; view[3] = height;

			GLES11.GlGetFloatv(GLES11.GlProjectionMatrix, matProjection, 0);
			GLES11.GlGetFloatv(GLES11.GlModelviewMatrix, matModelView, 0);
			GLES11.GlGetIntegerv(GLES11.GlViewport, view, 0);

			float[] unProject = new float[4];
			GLU.GluUnProject(0, 0, 0, matModelView, 0, matProjection, 0, view, 0, unProject, 0);

			LeftEdgeOfScreen = unProject[0] / unProject[3];
			TopEdgeOfScreen = -unProject[1] / unProject[3];

			GLU.GluUnProject(view[2], view[3], 0, matModelView, 0, matProjection, 0, view, 0, unProject, 0);

			RightEdgeOfScreen = unProject[0] / unProject[3];
			BottomEdgeOfScreen = -unProject[1] / unProject[3];

			if (LeftEdgeOfScreen > RightEdgeOfScreen)
			{
				float t = LeftEdgeOfScreen;
				LeftEdgeOfScreen = RightEdgeOfScreen;
				RightEdgeOfScreen = t;
			}
			if (BottomEdgeOfScreen > TopEdgeOfScreen)
			{
				float t = TopEdgeOfScreen;
				TopEdgeOfScreen = BottomEdgeOfScreen;
				BottomEdgeOfScreen = t;
			}

			/*			1
					    |
					    |
				  -r ---+--- r
					    |
					    |
					   -1
			*/
			ScreenBoundariesSet = true;
		}
	}
}