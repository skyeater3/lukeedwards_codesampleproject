using System;
using System.Diagnostics;

namespace App1
{
	public class Vector2f
	{
		public float x, y;

		[DebuggerStepThrough]
		public Vector2f(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		[DebuggerStepThrough]
		public Vector2f(Vector2f v)
		{
			this.x = v.x;
			this.y = v.y;
		}

		[DebuggerStepThrough]
		public static Vector2f operator -(Vector2f v1, Vector2f v2)
		{
			if (v1 == null || v2 == null)
				return null;

			return new Vector2f(v1.x - v2.x, v1.y - v2.y);
		}

		[DebuggerStepThrough]
		public static Vector2f operator -(Vector2f v1)
		{
			if (v1 == null)
				return null;

			return new Vector2f(-v1.x, -v1.y);
		}

		[DebuggerStepThrough]
		public static Vector2f operator +(Vector2f v1, Vector2f v2)
		{
			if (v1 == null || v2 == null)
				return null;

			return new Vector2f(v1.x + v2.x, v1.y + v2.y);
		}

		[DebuggerStepThrough]
		public static Vector2f operator *(Vector2f v, float f)
		{
			if (v == null)
				return null;
			return new Vector2f(v.x * f, v.y * f);
		}

		[DebuggerStepThrough]
		public static Vector2f operator /(Vector2f v, float f)
		{
			if (v == null)
				return null;
			return new Vector2f(v.x / f, v.y / f);
		}

		[DebuggerStepThrough]
		public float dot(Vector2f v)
		{
			if (v == null)
				return 0;

			return x * v.x + y * v.y;
		}
		
		public float Magnitude
		{
			[DebuggerStepThrough]
			get { return (float) Math.Sqrt(x * x + y * y); }
		}

		[DebuggerStepThrough]
		public Vector2f Abs()
		{
			return new Vector2f(Math.Abs(x), Math.Abs(y));
		}

		[DebuggerStepThrough]
		public Vector2f Normalise()
		{
			return new Vector2f(x / Magnitude, y / Magnitude);
		}

		[DebuggerStepThrough]
		public static Vector2f Reflect(Vector2f v, Vector2f n)
		{
			return new Vector2f(v - n * v.dot(n) * 2);
		}

		[DebuggerStepThrough]
		public static Vector2f PerpendicularClockwise(Vector2f v)
		{
			if (v == null) return null;
			return new Vector2f(-v.y, v.x);
		}

		[DebuggerStepThrough]
		public static Vector2f PerpendicularCounterClockwise(Vector2f v)
		{
			if (v == null) return null;
			return new Vector2f(v.y, -v.x);
		}

		[DebuggerStepThrough]
		public static Vector2f PerpendicularClockwise(float x, float y)
		{
			return new Vector2f(-y, x);
		}

		[DebuggerStepThrough]
		public static Vector2f PerpendicularCounterClockwise(float x, float y)
		{
			return new Vector2f(y, -x);
		}
	}
}