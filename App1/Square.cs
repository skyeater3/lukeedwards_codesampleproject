﻿using Android.Opengl;
using Java.Nio;
using Javax.Microedition.Khronos.Opengles;

namespace App1
{
	/**
	 * A two-dimensional square for use as a drawn object in OpenGL ES 1.0/1.1.
	 */

	public class Square
	{
		private readonly FloatBuffer vertexBuffer;
		private readonly ShortBuffer drawListBuffer;

		// number of coordinates per vertex in this array
		private static readonly int COORDS_PER_VERTEX = 3;

		private static float[] squareCoords = {
			-0.5f,  0.5f, 0.0f,   // top left
			-0.5f, -0.5f, 0.0f,   // bottom left
				0.5f, -0.5f, 0.0f,   // bottom right
				0.5f,  0.5f, 0.0f }; // top right

		private readonly short[] drawOrder = { 0, 1, 2, 0, 2, 3 }; // order to draw vertices

		private readonly float[] defaultColor = { 0.2f, 0.709803922f, 0.898039216f, 1.0f };

		/**
		 * Sets up the drawing object data for use in an OpenGL ES context.
		 */

		public Square()
		{
			// initialize vertex byte buffer for shape coordinates
			ByteBuffer bb = ByteBuffer.AllocateDirect(
					// (# of coordinate values * 4 bytes per float)
					squareCoords.Length * 4);
			bb.Order(ByteOrder.NativeOrder());
			vertexBuffer = bb.AsFloatBuffer();
			vertexBuffer.Put(squareCoords);
			vertexBuffer.Position(0);

			// initialize byte buffer for the draw list
			ByteBuffer dlb = ByteBuffer.AllocateDirect(
					// (# of coordinate values * 2 bytes per short)
					drawOrder.Length * 2);
			dlb.Order(ByteOrder.NativeOrder());
			drawListBuffer = dlb.AsShortBuffer();
			drawListBuffer.Put(drawOrder);
			drawListBuffer.Position(0);
		}

		/**
		 * Encapsulates the OpenGL ES instructions for drawing this shape.
		 *
		 * @param gl - The OpenGL ES context in which to draw this shape.
		 */

		public void Draw(IGL10 gl, float[] color = null)
		{
			if (color == null)
				color = defaultColor;

			// Since this shape uses vertex arrays, enable them
			GLES11.GlEnableClientState(GL11.GlVertexArray);

			// draw the shape
			GLES11.GlColor4f(       // set color
					color[0], color[1],
					color[2], color[3]);
			GLES11.GlVertexPointer( // point to vertex data:
					COORDS_PER_VERTEX,
					GL10.GlFloat, 0, vertexBuffer);
			GLES11.GlDrawElements(  // draw shape:
					GL10.GlTriangles,
					drawOrder.Length, GL11.GlUnsignedShort,
					drawListBuffer);

			// Disable vertex array drawing to avoid
			// conflicts with shapes that don't use it
			GLES11.GlDisableClientState(GL11.GlVertexArray);
		}
	}
}