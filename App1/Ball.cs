using Android.Opengl;
using Javax.Microedition.Khronos.Opengles;
using System;
using System.Diagnostics;

namespace App1
{
	public class Ball : IPhysicsCapableObject, IDrawableObject
	{
		private static readonly float LAUNCH_VELOCITY = 0.4f;

		private Vector2f position = new Vector2f(0, 0);
		private Vector2f size = new Vector2f(.025f, .025f);
		private Vector2f velocity = new Vector2f(0, 0);

		private bool ballIsAttachedToPaddle;
		public bool BallIsAttachedToPaddle => ballIsAttachedToPaddle;
		private bool isPhysicsActive = true;
		private bool isStatic = false;

		Vector2f IPhysicsCapableObject.Position
		{
			[DebuggerStepThrough]
			get
			{
				return position;
			}

			[DebuggerStepThrough]
			set
			{
				position = value;
			}
		}

		Vector2f IPhysicsCapableObject.Size
		{
			[DebuggerStepThrough]
			get
			{
				return size;
			}

			[DebuggerStepThrough]
			set
			{
				size = value;
			}
		}

		Vector2f IPhysicsCapableObject.Velocity
		{
			[DebuggerStepThrough]
			get
			{
				return velocity;
			}

			[DebuggerStepThrough]
			set
			{
				velocity = value;
			}
		}

		Physics.BoundingShapeType IPhysicsCapableObject.BoundingShape
		{
			get
			{
				return Physics.BoundingShapeType.Circle;
			}
		}

		bool IPhysicsCapableObject.IsPhysicsActive
		{
			[DebuggerStepThrough]
			get
			{
				return isPhysicsActive;
			}

			[DebuggerStepThrough]
			set
			{
				isPhysicsActive = value;
			}
		}

		bool IPhysicsCapableObject.IsStatic
		{
			[DebuggerStepThrough]
			get
			{
				return isStatic;
			}

			[DebuggerStepThrough]
			set
			{
				isStatic = value;
			}
		}

		private BallType ballType;
		private Paddle paddle;
		private Vector2f attachPoint;

		private Circle circle;

		public enum BallType
		{
			Normal
		}

		public Ball(BallType ballType, Paddle paddle = null)
		{
			this.ballType = ballType;
			if (paddle != null)
			{
				SetInitialAttachPoint(paddle, new Vector2f(0.4f, size.x));
			}
			circle = new Circle(20);
		}

		public Ball(BallType ballType, Vector2f position, Vector2f velocity, Vector2f size)
		{
			this.ballType = ballType;
			this.position = position;
			this.velocity = velocity;
			this.size = size;
			circle = new Circle(20);
		}

		public void AttachToPaddle(Paddle paddle)
		{
			if (!ballIsAttachedToPaddle && paddle != null)
			{
				ballIsAttachedToPaddle = true;
				isStatic = true;
				isPhysicsActive = false;
				this.paddle = paddle;
				this.attachPoint = position - (paddle as IPhysicsCapableObject).Position;
			}
		}

		public void SetInitialAttachPoint(Paddle paddle, Vector2f attachPoint)
		{
			if (paddle != null)
			{
				AttachToPaddle(paddle);

				var paddleAsPhys = paddle as IPhysicsCapableObject;
				this.attachPoint.x = attachPoint.x * paddleAsPhys.Size.x / 2f;
				this.attachPoint.y = attachPoint.y + Math.Sign(attachPoint.y) * (paddleAsPhys.Size.y / 2f + 0.0001f);	// tiny increment to make sure the ball is not inside the paddle
			}
		}

		public bool Launch()
		{
			if (ballIsAttachedToPaddle && paddle != null)
			{
				var paddlePhysics = paddle as IPhysicsCapableObject;

				var launchVector = attachPoint.Normalise();

				velocity = launchVector.Normalise() * LAUNCH_VELOCITY;
				position += velocity * 0.01f;	// fake dt. Just to get the ball off the paddle before update. was having some problem with collisions rebounding to inside of the paddle

				ballIsAttachedToPaddle = false;
				isPhysicsActive = true;
				isStatic = false;
				paddle = null;
				return true;
			}
			return false;
		}

		void IDrawableObject.Draw(IGL10 gl)
		{
			GLES11.GlPushMatrix();

			GLES11.GlTranslatef(-position.x, position.y, 0);
			GLES11.GlScalef(size.x, size.x, size.x);

			circle.Draw(gl);
			GLES11.GlPopMatrix();
		}

		void IPhysicsCapableObject.UpdateWithCollisionResolve(double dt, Collision collision)
		{
			if (collision == null)
			{
				Update(dt);
			}
			else
			{
				Collide(collision.collisionWith, collision, dt);
			}
		}

		public void Update(double dt)
		{
			if (ballIsAttachedToPaddle && paddle != null)
			{
				var paddlePhysics = paddle as IPhysicsCapableObject;
				var paddlePosition = paddlePhysics.Position;
				var paddleSize = paddlePhysics.Size;

				position = paddlePosition + attachPoint;
			}
			else
			{
				position += velocity * (float) dt;

				if (position.x < ScreenBoundaries.LeftEdgeOfScreen + size.x + float.Epsilon)
				{
					velocity.x = -velocity.x;
					position.x = ScreenBoundaries.LeftEdgeOfScreen + size.x;
				}
				else if (position.x > ScreenBoundaries.RightEdgeOfScreen - size.x - float.Epsilon)
				{
					velocity.x = -velocity.x;
					position.x = ScreenBoundaries.RightEdgeOfScreen - size.x;
				}

				if (position.y > ScreenBoundaries.TopEdgeOfScreen - size.x - float.Epsilon)
				{
					velocity.y = -velocity.y;
					position.y = ScreenBoundaries.TopEdgeOfScreen - size.x;
				}
				else if (position.y < ScreenBoundaries.BottomEdgeOfScreen - size.x + float.Epsilon)
				{
					PhysicsUpdate.PhysicsUpdateObject.DestroyPhysicsObject(this);
					//velocity.y = -velocity.y;
					//position.y = ScreenBoundaries.BottomEdgeOfScreen - size.x;
				}
			}
		}

		public void Collide(IPhysicsCapableObject pco, Collision collision, double dt)
		{
			Paddle paddle = pco as Paddle;
			if (paddle != null)
			{
				if (!ballIsAttachedToPaddle)
				{
					collision.ResolvePositionAndVelocity(ref position, ref velocity, dt);
				}
				return;
			}

			TargetBox tb = pco as TargetBox;
			if (tb != null)
			{
				collision.ResolvePositionAndVelocity(ref position, ref velocity, dt);

				return;
			}
		}
	}
}