namespace App1
{
	public interface IPhysicsCapableObject
	{
		bool IsPhysicsActive { get; set; }
		bool IsStatic { get; set; }
		Vector2f Position { get; set; }
		Vector2f Size { get; set; }
		Vector2f Velocity { get; set; }

		Physics.BoundingShapeType BoundingShape { get; }

		
		void Update(double dt);
				
		void Collide(IPhysicsCapableObject pco, Collision collision, double dt);
		void UpdateWithCollisionResolve(double dt, Collision collision);	// resolving these seperately was causing double movement.
																			// so I combined them, but then didn't remove the old functions in most cases.
	}
}