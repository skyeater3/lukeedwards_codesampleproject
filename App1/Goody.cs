using Android.Opengl;
using Javax.Microedition.Khronos.Opengles;
using System.Diagnostics;
using System;

namespace App1
{
	public class Goody : IPhysicsCapableObject, IDrawableObject
	{
		Physics.BoundingShapeType IPhysicsCapableObject.BoundingShape
		{
			[DebuggerStepThrough]
			get
			{
				return Physics.BoundingShapeType.Circle;
			}
		}

		private bool isPhysicsActive = true;

		bool IPhysicsCapableObject.IsPhysicsActive
		{
			[DebuggerStepThrough]
			get
			{
				return isPhysicsActive;
			}

			[DebuggerStepThrough]
			set
			{
				isPhysicsActive = value;
			}
		}

		private bool isStatic = false;

		bool IPhysicsCapableObject.IsStatic
		{
			[DebuggerStepThrough]
			get
			{
				return isStatic;
			}

			[DebuggerStepThrough]
			set
			{
				isStatic = value;
			}
		}

		private Vector2f position = new Vector2f(0, 0);

		Vector2f IPhysicsCapableObject.Position
		{
			[DebuggerStepThrough]
			get
			{
				return position;
			}

			[DebuggerStepThrough]
			set
			{
				position = value;
			}
		}

		private Vector2f size = new Vector2f(0.05f, 0.05f);

		Vector2f IPhysicsCapableObject.Size
		{
			[DebuggerStepThrough]
			get
			{
				return size;
			}

			[DebuggerStepThrough]
			set
			{
				size = value;
			}
		}

		private Vector2f velocity = new Vector2f(0, 0);

		Vector2f IPhysicsCapableObject.Velocity
		{
			[DebuggerStepThrough]
			get
			{
				return velocity;
			}

			[DebuggerStepThrough]
			set
			{
				velocity = value;
			}
		}

		private Circle circle;
		private float[] color;

		public enum GoodyType
		{
			Slow,

			//Guns,		// can do this sort of stuff later if I have time
			MultiBall,

			Fast,
			LargePaddle,
			SmallPaddle,
			Coin,
			StickyPaddle
		}

		private GoodyType goodyType;
		private float GOODY_SPEED = 0.5f;

		public Goody(GoodyType goodyToDrop, Vector2f position)
		{
			this.position = position;
			goodyType = goodyToDrop;
			velocity = new Vector2f(0, -1) * GOODY_SPEED;
			circle = new Circle(20);

			switch (goodyToDrop)
			{
				case GoodyType.Slow:
					color = new float[] { 0.0f, 0.0f, 1.0f, 1.0f };
					break;
				//case GoodyType.Guns:
				//	color = new float[] { 0.0f, 0.0f, 1.0f, 1.0f };
				//	break;
				case GoodyType.MultiBall:
					color = new float[] { 0.5f, 0.5f, 0.5f, 1.0f };
					break;

				case GoodyType.Fast:
					color = new float[] { 1.0f, 1.0f, 0.0f, 1.0f };
					break;

				case GoodyType.LargePaddle:
					color = new float[] { 0.0f, 1.0f, 1.0f, 1.0f };
					break;

				case GoodyType.SmallPaddle:
					color = new float[] { 1.0f, 0.0f, 0.0f, 1.0f };
					break;

				case GoodyType.Coin:
					color = null;   // useDefaultColor
					break;

				case GoodyType.StickyPaddle:
					color = new float[] { 0.0f, 1.0f, 0.0f, 1.0f };
					break;

				default:
					break;
			}
		}

		public void Update(double dt)
		{
			position += velocity * (float) dt;

			if (position.y + size.y / 2f < ScreenBoundaries.BottomEdgeOfScreen ||
				position.y - size.y / 2f > ScreenBoundaries.TopEdgeOfScreen)
				PhysicsUpdate.PhysicsUpdateObject.DestroyPhysicsObject(this);
		}

		private void GiveGoody()
		{
			switch (goodyType)
			{
				case GoodyType.Slow:
					PhysicsUpdate.PhysicsUpdateObject.ReduceVelocityOfAllBalls();
					break;

				case GoodyType.MultiBall:
					PhysicsUpdate.PhysicsUpdateObject.SpawnNewBalls();
					break;

				case GoodyType.Fast:
					PhysicsUpdate.PhysicsUpdateObject.IncreaseVelocityOfAllBalls();
					break;

				case GoodyType.LargePaddle:
					PhysicsUpdate.PhysicsUpdateObject.MakePaddleLarge();
					break;

				case GoodyType.SmallPaddle:
					PhysicsUpdate.PhysicsUpdateObject.MakePaddleSmall();
					break;

				case GoodyType.Coin:
					GameStateLoop.GameStateLoopInstance.AddPoints(140);
					break;

				case GoodyType.StickyPaddle:
					PhysicsUpdate.PhysicsUpdateObject.MakePaddleSticky();
					break;

				default:
					break;
			}
		}

		public void Collide(IPhysicsCapableObject pco, Collision collision, double dt)
		{
			Paddle paddle = pco as Paddle;
			if (paddle != null)
			{
				GiveGoody();
				//Console.WriteLine("Collected goody " + goodyType);

				PhysicsUpdate.PhysicsUpdateObject.DestroyPhysicsObject(this);

				GameStateLoop.GameStateLoopInstance.AddPoints(10);
			}
		}

		void IDrawableObject.Draw(IGL10 gl)
		{
			GLES11.GlPushMatrix();

			GLES11.GlTranslatef(-position.x, position.y, 0);
			GLES11.GlScalef(size.x, size.x, size.x);

			circle.Draw(gl, color);
			GLES11.GlPopMatrix();
		}

		void IPhysicsCapableObject.UpdateWithCollisionResolve(double dt, Collision collision)
		{
			Update(dt);
			if (collision != null)
				Collide(collision.collisionWith, collision, dt);
		}
	}
}