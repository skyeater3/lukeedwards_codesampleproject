using Android.Opengl;
using Javax.Microedition.Khronos.Opengles;
using System;
using System.Diagnostics;

namespace App1
{
	public class TargetBox : IPhysicsCapableObject, IDrawableObject
	{
		Physics.BoundingShapeType IPhysicsCapableObject.BoundingShape
		{
			[DebuggerStepThrough]
			get
			{
				return Physics.BoundingShapeType.Rect;
			}
		}

		private bool isPhysicsActive = true;

		bool IPhysicsCapableObject.IsPhysicsActive
		{
			[DebuggerStepThrough]
			get
			{
				return isPhysicsActive;
			}

			[DebuggerStepThrough]
			set
			{
				isPhysicsActive = value;
			}
		}

		private bool isStatic = true;

		bool IPhysicsCapableObject.IsStatic
		{
			[DebuggerStepThrough]
			get
			{
				return isStatic;
			}

			[DebuggerStepThrough]
			set
			{
				isStatic = value;
			}
		}

		private Vector2f position = new Vector2f(0, 0);

		Vector2f IPhysicsCapableObject.Position
		{
			[DebuggerStepThrough]
			get
			{
				return position;
			}

			[DebuggerStepThrough]
			set
			{
				position = value;
			}
		}

		private Vector2f size = new Vector2f(0.1f, 0.1f);

		Vector2f IPhysicsCapableObject.Size
		{
			[DebuggerStepThrough]
			get
			{
				return size;
			}

			[DebuggerStepThrough]
			set
			{
				size = value;
			}
		}

		private Vector2f velocity = new Vector2f(0, 0);

		Vector2f IPhysicsCapableObject.Velocity
		{
			[DebuggerStepThrough]
			get
			{
				return velocity;
			}

			[DebuggerStepThrough]
			set
			{
				velocity = value;
			}
		}

		private int hitpoints;
		private int initialHitpoints;
		private bool dropsGoodies;

		private Square square;
		private float[] color;

		public TargetBox(Vector2f position, int hitpoints = 1)
		{
			////sets pos to bottom of screen (centre overlaps the edge of screen)
			//position.y = ScreenBoundaries.BottomEdgeOfScreen; ;

			//// adds the entire size of the screen to the position (centre overlaps the top edge of the screen)
			////position.y += (ScreenBoundaries.TopEdgeOfScreen - ScreenBoundaries.BottomEdgeOfScreen);

			//// moves paddle to 20% of screen height
			//position.y += (ScreenBoundaries.TopEdgeOfScreen - ScreenBoundaries.BottomEdgeOfScreen) * .2f;
			this.position = position;
			square = new Square();
			color = new float[] { RandomValue.GetRandomInt(0, 255) / 255f, RandomValue.GetRandomInt(0, 255) / 255f, RandomValue.GetRandomInt(0, 255) / 255f, RandomValue.GetRandomInt(0, 255) / 255f };
			this.hitpoints = hitpoints;
			initialHitpoints = hitpoints;

			dropsGoodies = RandomValue.GetRandomInt(1, 3) == 2;
		}

		//void IPhysicsCapableObject.Collide(IPhysicsCapableObject pco, Physics.IntersectionResult intersection, double dt)
		public void Collide(IPhysicsCapableObject pco, Collision collision, double dt)
		{
			Ball b = pco as Ball;
			if (b != null)
			{
				hitpoints -= 1;

				if (hitpoints <= 0)
				{
					PhysicsUpdate.PhysicsUpdateObject.DestroyPhysicsObject(this);

					if (dropsGoodies)
					{
						var goodyToDrop = RandomValue.GetRandomInt(Enum.GetValues(typeof(Goody.GoodyType)).Length);
						PhysicsUpdate.PhysicsUpdateObject.AddPhysicsObject(new Goody((Goody.GoodyType) goodyToDrop, position));
					}

					GameStateLoop.GameStateLoopInstance.AddPoints(10 * initialHitpoints);
				}				
			}
		}

		void IPhysicsCapableObject.Update(double dt)
		{
			//throw new NotImplementedException();
		}

		void IDrawableObject.Draw(IGL10 gl)
		{
			GLES11.GlPushMatrix();

			GLES11.GlTranslatef(-position.x, position.y, 0);
			GLES11.GlScalef(size.x, size.y, 1);
			square.Draw(gl, color);

			GLES11.GlPopMatrix();
		}

		void IPhysicsCapableObject.UpdateWithCollisionResolve(double dt, Collision collision)
		{
			if (collision != null)
				Collide(collision.collisionWith, collision, dt);
		}
	}
}