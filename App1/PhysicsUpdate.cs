using App1;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App1
{
	public class PhysicsUpdate
	{
		private static PhysicsUpdate physicsUpdateObject = null;

		private PhysicsUpdate()
		{
		}

		public static PhysicsUpdate PhysicsUpdateObject
		{
			get
			{
				if (physicsUpdateObject == null)
					physicsUpdateObject = new PhysicsUpdate();
				return physicsUpdateObject;
			}
		}

		private List<IPhysicsCapableObject> allPhysicsObjects = new List<IPhysicsCapableObject>();
		private List<IPhysicsCapableObject> markedForDestroy = new List<IPhysicsCapableObject>();
		private List<IPhysicsCapableObject> batchAdd = new List<IPhysicsCapableObject>();
		private List<IPhysicsCapableObject> balls = new List<IPhysicsCapableObject>();
		public bool noBalls = false;
		public bool noBoxes = false;
		private float movePaddleX = 0f;

		public void AddPhysicsObject(IPhysicsCapableObject pco)
		{
			allPhysicsObjects.Add(pco);
		}

		public void DestroyPhysicsObject(IPhysicsCapableObject pco)
		{
			markedForDestroy.Add(pco);
		}

		public void ClearAllPhysicsObjects()
		{
			allPhysicsObjects.Clear();
			markedForDestroy.Clear();
			batchAdd.Clear();
			noBalls = true;
			noBoxes = true;
		}

		public void UpdateAll()
		{
			noBalls = !allPhysicsObjects.Any((pco) => pco is Ball);
			noBoxes = !allPhysicsObjects.Any((pco) => pco is TargetBox);

			if (!noBalls && !noBoxes)
			{
				double dt = DeltaTime.GetDeltaTime() / 1000;

				//CheckForCollisions(dt);
				CheckForCollisionsAndUpdate(dt);

				//foreach (var pco in allPhysicsObjects)
				for (int i = 0; i < allPhysicsObjects.Count; i++)
				{
					if (allPhysicsObjects[i].IsStatic)
						allPhysicsObjects[i].Update(dt);

					if (allPhysicsObjects[i] is IDrawableObject)
					{
						((IDrawableObject) allPhysicsObjects[i]).Draw(null);
					}

					if (allPhysicsObjects[i] is Paddle)
					{
						((Paddle) allPhysicsObjects[i]).MovePaddleWithTilt(movePaddleX, (float) dt);
					}
				}

				for (int i = 0; i < markedForDestroy.Count; i++)
				{
					allPhysicsObjects.Remove(markedForDestroy[i]);
				}
				markedForDestroy.Clear();
				for (int i = 0; i < batchAdd.Count; i++)
				{
					AddPhysicsObject(batchAdd[i]);
				}
				batchAdd.Clear();
			}
		}

		public void CheckForCollisionsAndUpdate(double dt)
		{
			var staticPCOs = allPhysicsObjects.Where((pco) => pco.IsStatic && pco.IsPhysicsActive).ToList();
			var movingPCOs = allPhysicsObjects.Where((pco) => pco.IsPhysicsActive && !pco.IsStatic).ToList();


			for (int i = 0; i < movingPCOs.Count; i++)
			{
				bool anyCollision = false;
				IPhysicsCapableObject currentPCO = movingPCOs[i];

				// test for collisions between moving objects
				for (int j = i + 1; j < movingPCOs.Count; j++)
				{
					IPhysicsCapableObject otherPCO = movingPCOs[j];
					if (currentPCO.BoundingShape == Physics.BoundingShapeType.Rect)		// nonStatic rect is paddle
					{
						if (otherPCO.BoundingShape == Physics.BoundingShapeType.Circle)
						{
							var collision = PhysicsSphereRectCollision.CircleRectCollisionHandler(currentPCO, otherPCO, (float) dt);
							if (collision != null)
							{
								collision.collisionWith = currentPCO;
								otherPCO.UpdateWithCollisionResolve(dt, collision);

								collision.collisionWith = otherPCO;
								currentPCO.UpdateWithCollisionResolve(dt, collision);

								//currentPCO.Collide(otherPCO, collision, dt);
								//otherPCO.Collide(currentPCO, collision, dt);
								anyCollision = true;
							}
						}
					}
					else // if (currentPCO.BoundingShape == Physics.BoundingShapeType.Circle) // unnecessary test -- only two boundingshapetypes
					{
						if (otherPCO.BoundingShape == Physics.BoundingShapeType.Rect)	// this is probably unreachable code (paddle is created first)
						{
							var collision = PhysicsSphereRectCollision.CircleRectCollisionHandler(otherPCO, currentPCO, (float) dt); ;
							if (collision != null)
							{
								collision.collisionWith = currentPCO;
								otherPCO.UpdateWithCollisionResolve(dt, collision);

								collision.collisionWith = otherPCO;
								currentPCO.UpdateWithCollisionResolve(dt, collision);					

								//currentPCO.Collide(otherPCO, collision, dt);
								//otherPCO.Collide(currentPCO, collision, dt);
								anyCollision = true;
							}
						}
					}
				}

				if (!(currentPCO is Goody))	// ignore collisions with static objects for falling goodies
				{
					// test for collisions with all static objects
					for (int j = 0; j < staticPCOs.Count; j++)
					{
						IPhysicsCapableObject otherPCO = staticPCOs[j];
						// not bothering to test for paddle & static rect collisions, or circle & static circle collisions (because the only static circles are balls attached to paddle)
						if (currentPCO.BoundingShape == Physics.BoundingShapeType.Circle && otherPCO.BoundingShape == Physics.BoundingShapeType.Rect)
						{
							var collision = PhysicsSphereRectCollision.CircleRectCollisionHandler(otherPCO, currentPCO, (float) dt); ;
							if (collision != null)
							{
								collision.collisionWith = otherPCO;
								currentPCO.UpdateWithCollisionResolve(dt, collision);

								collision.collisionWith = currentPCO;
								otherPCO.UpdateWithCollisionResolve(dt, collision);
								//currentPCO.Collide(otherPCO, collision, dt);
								//otherPCO.Collide(currentPCO, collision, dt);
								anyCollision = true;
							}
						}
					}
				}

				if (!anyCollision)
					currentPCO.UpdateWithCollisionResolve(dt, null);
			}				
		}

		public void MovePaddleWithTilt(float x)
		{
			movePaddleX = x;
		}

		public void ReduceVelocityOfAllBalls()
		{
			foreach (var pco in allPhysicsObjects)
			{
				if (pco is Ball)
				{
					pco.Velocity *= 0.6f;
				}
			}
		}

		public void IncreaseVelocityOfAllBalls()
		{
			foreach (var pco in allPhysicsObjects)
			{
				if (pco is Ball)
				{
					pco.Velocity *= 1.3f;
				}
			}
		}

		public void SpawnNewBalls()
		{
			foreach (var pco in allPhysicsObjects)
			{
				if (pco is Ball)
				{
					batchAdd.Add(new Ball(Ball.BallType.Normal, pco.Position, Vector2f.PerpendicularClockwise(pco.Velocity), pco.Size));
				}
			}
		}

		public void MakePaddleSticky()
		{
			foreach (var pco in allPhysicsObjects)
			{
				if (pco is Paddle)
				{
					Paddle paddle = (Paddle) pco;
					paddle.MakeSticky(true);
				}
			}
		}

		public void MakePaddleLarge()
		{
			foreach (var pco in allPhysicsObjects)
			{
				if (pco is Paddle)
				{
					Paddle paddle = (Paddle) pco;
					paddle.ChangePaddleSize(Paddle.largeSize);
				}
			}
		}

		public void MakePaddleSmall()
		{
			foreach (var pco in allPhysicsObjects)
			{
				if (pco is Paddle)
				{
					Paddle paddle = (Paddle) pco;
					paddle.ChangePaddleSize(Paddle.smallSize);
				}
			}
		}

		public void LaunchAttachedBalls()
		{
			bool launchedAny = false;
			Paddle paddle = null;

			for (int i = 0; i < allPhysicsObjects.Count; i++)
			{
				var ball = allPhysicsObjects[i] as Ball;
				if (ball != null)
					launchedAny = launchedAny | ball.Launch();
				else if (allPhysicsObjects[i] is Paddle)
					paddle = (Paddle) allPhysicsObjects[i];
			}

			if (launchedAny && paddle.ShouldNotBeStickyAfterLaunch)
			{
				paddle.MakeSticky(false);
			}
		}
	}

	public static class Physics
	{
		public enum BoundingShapeType
		{
			Rect,
			Circle
		}
	}	
}