using Android.Opengl;
using Javax.Microedition.Khronos.Opengles;
using System;
using System.Diagnostics;

namespace App1
{
	public class Paddle : IPhysicsCapableObject, IDrawableObject
	{
		public static readonly Vector2f defaultSize = new Vector2f(.2f, .05f);
		public static readonly Vector2f largeSize = new Vector2f(.3f, .05f);
		public static readonly Vector2f smallSize = new Vector2f(.1f, .05f);
		private Vector2f size = new Vector2f(.2f, .05f);
		private Vector2f position;// = new Vector2f(0f,0f);
		private Vector2f velocity;// = new Vector2f(0f,0f);

		private float speed = 0.5f;

		private const float MINIMUM_TILT = 0.5f;
		private const float DECELERATION = 0.5f;

		private Square square;

		private bool isPhysicsActive = true;
		private bool isStatic = false;
		private bool isSticky = true;
		public bool IsSticky { get { return isSticky; } }
		public bool ShouldNotBeStickyAfterLaunch { get { return goodyTimer[(int) Goody.GoodyType.StickyPaddle] <= 0.0f; } }

		private float[] goodyTimer;

		Vector2f IPhysicsCapableObject.Position
		{
			[DebuggerStepThrough]
			get
			{
				return position;
			}

			[DebuggerStepThrough]
			set
			{
				position = value;
			}
		}

		Vector2f IPhysicsCapableObject.Size
		{
			[DebuggerStepThrough]
			get
			{
				return size;
			}

			[DebuggerStepThrough]
			set
			{
				size = value;
			}
		}

		Vector2f IPhysicsCapableObject.Velocity
		{
			[DebuggerStepThrough]
			get
			{
				return velocity;
			}

			[DebuggerStepThrough]
			set
			{
				velocity = value;
			}
		}

		Physics.BoundingShapeType IPhysicsCapableObject.BoundingShape
		{
			[DebuggerStepThrough]
			get
			{
				return Physics.BoundingShapeType.Rect;
			}
		}

		bool IPhysicsCapableObject.IsPhysicsActive
		{
			[DebuggerStepThrough]
			get
			{
				return isPhysicsActive;
			}

			[DebuggerStepThrough]
			set
			{
				isPhysicsActive = value;
			}
		}

		bool IPhysicsCapableObject.IsStatic
		{
			[DebuggerStepThrough]
			get
			{
				return isStatic;
			}

			[DebuggerStepThrough]
			set
			{
				isStatic = value;
			}
		}

		public Paddle()
		{
			goodyTimer = new float[Enum.GetNames(typeof(Goody.GoodyType)).Length];
			for (int i = 0; i < goodyTimer.Length; i++)
			{
				goodyTimer[i] = 0.0f;
			}

			square = new Square();
			position = new Vector2f(0, 0);
			velocity = new Vector2f(0, 0);
		}

		public void ChangeY()
		{
			if (ScreenBoundaries.ScreenBoundariesSet) // change start y position to 20% above bottom screen edge.
			{
				//sets pos to bottom of screen (centre overlaps the edge of screen)
				position.y = ScreenBoundaries.BottomEdgeOfScreen; ;

				// adds the entire size of the screen to the position (centre overlaps the top edge of the screen)
				//position.y += (ScreenBoundaries.TopEdgeOfScreen - ScreenBoundaries.BottomEdgeOfScreen);

				// moves paddle to 20% of screen height
				position.y += (ScreenBoundaries.TopEdgeOfScreen - ScreenBoundaries.BottomEdgeOfScreen) * .2f;
				// corrects for size of paddle
				float paddleSize = (size.y / 2f);
				position.y += (Math.Sign(position.y) > 0) ? -paddleSize : paddleSize;
			}
		}

		void IDrawableObject.Draw(IGL10 gl)
		{
			GLES11.GlPushMatrix();

			GLES11.GlTranslatef(-position.x, position.y, 0);
			GLES11.GlScalef(size.x, size.y, 1);
			square.Draw(gl);

			GLES11.GlPopMatrix();
		}

		public void MovePaddleWithTilt(float x, float dt)
		{
			// logic here was, we aren't actually moving the paddle with the velocity.
			// although it is functionally the same.
			// just storing the fixed velocity for use in the collision detection later
			velocity.x = -(Math.Abs(x) > MINIMUM_TILT ? x : 0) * speed;
			position.x -= (Math.Abs(x) > MINIMUM_TILT ? x : 0) * speed * dt;

			if (ScreenBoundaries.ScreenBoundariesSet)
			{
				float leftEdgeOfPaddle = position.x - (size.x / 2f);
				float rightEdgeOfPaddle = position.x + (size.x / 2f);

				if (leftEdgeOfPaddle < ScreenBoundaries.LeftEdgeOfScreen + float.Epsilon)
				{
					position.x = (ScreenBoundaries.LeftEdgeOfScreen + (size.x / 2f));
					velocity.x = 0;
				}
				else if (rightEdgeOfPaddle > ScreenBoundaries.RightEdgeOfScreen - float.Epsilon)
				{
					position.x = (ScreenBoundaries.RightEdgeOfScreen - (size.x / 2f));
					velocity.x = 0;
				}
			}
		}

		public void Update(double dt)
		{
			for (int i = 0; i < goodyTimer.Length; i++)
			{
				if (goodyTimer[i] > 0)
				{
					goodyTimer[i] -= (float) dt;
				}

				if (goodyTimer[i] < 0)
				{
					goodyTimer[i] = 0.0f;
					Goody.GoodyType type = (Goody.GoodyType) i;
					switch (type)
					{
						case Goody.GoodyType.LargePaddle:
							ChangePaddleSize(defaultSize);
							break;

						case Goody.GoodyType.SmallPaddle:
							ChangePaddleSize(defaultSize);
							break;

						case Goody.GoodyType.StickyPaddle:
							MakeSticky(false);
							break;

						default:
							break;
					}
				}
			}
		}

		public void MakeSticky(bool sticky)
		{
			goodyTimer[(int) Goody.GoodyType.StickyPaddle] = 10.0f;
			isSticky = sticky;
		}

		public void ChangePaddleSize(Vector2f size)
		{
			goodyTimer[(int) Goody.GoodyType.LargePaddle] = 10.0f;
			this.size = size;
		}

		public void Collide(IPhysicsCapableObject pco, Collision collision, double dt)
		{
			Ball b = pco as Ball;
			if (b != null)
			{
				if (isSticky)
				{
					pco.Position = collision.CollisionLocation;
					b.AttachToPaddle(this);
				}

				GameStateLoop.GameStateLoopInstance.AddPoints(1);
			}
		}

		void IPhysicsCapableObject.UpdateWithCollisionResolve(double dt, Collision collision)
		{
			Update(dt);
			if (collision != null)
				Collide(collision.collisionWith, collision, dt);
		}
	}
}