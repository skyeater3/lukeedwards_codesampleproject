using Javax.Microedition.Khronos.Opengles;

namespace App1
{
	public interface IDrawableObject
	{
		void Draw(IGL10 gl);
	}
}