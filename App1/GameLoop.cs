using Android.Graphics;
using Android.Views;
using Android.Widget;
using System;

namespace App1
{
	public class GameStateLoop : Java.Lang.Object, ISurfaceHolderCallback
	{
		private PhysicsUpdate physics;

		private Paddle paddle;

		private bool stateLoopRunning = false;
		private bool stateLoopPaused = false;

		private static GameStateLoop gameStateLoop;

		private GameStateLoop()
		{
		}

		public static GameStateLoop GameStateLoopInstance { get { if (gameStateLoop == null) gameStateLoop = new GameStateLoop(); return gameStateLoop; } }

		private int screenWidth, screenHeight;
		private bool surfaceReady = false;
		private bool awaitingStart = false;

		private static int score = 0;
		private static TextView scoreTextView = null;

		public enum State
		{
			Initialising,
			Menu_Main,
			Menu_About,
			Game_Loading,
			Game_Play,
			Game_Pause,
			Game_End,
			Highscores
		}

		private State state = State.Initialising;
		public bool stateFinalised = false;

		public void AddPoints(int points)
		{
			score += points;
		}

		public void StartStateLoop()
		{
			if (surfaceReady)
			{
				if (!stateLoopRunning)
				{
					switch (state)
					{
						case State.Initialising:
							ChangeState(State.Menu_Main);
							break;

						case State.Menu_Main:
							break;

						case State.Menu_About:
							break;

						case State.Game_Loading:
							break;

						case State.Game_Play:
							break;

						case State.Game_Pause:
							break;

						case State.Game_End:
							break;

						case State.Highscores:
							break;

						default:
							break;
					}

					awaitingStart = false;
					stateLoopRunning = true;
					stateLoopPaused = false;
				}
			}
		}

		public void InitialiseState(State stateToInit)
		{
			this.state = stateToInit;

			switch (stateToInit)
			{
				case State.Initialising:
					break;

				case State.Menu_Main:
					MenuControls.AddMainMenuView();
					break;

				case State.Menu_About:
					MenuControls.AddAboutMenuView();
					break;

				case State.Game_Loading:
					score = 0;

					if (physics == null)
					{
						physics = PhysicsUpdate.PhysicsUpdateObject;
					}
					physics.ClearAllPhysicsObjects();

					if (paddle != null)
					{
						paddle = null;
					}

					paddle = new Paddle();
					paddle.ChangeY();

					physics.AddPhysicsObject(paddle);
					physics.AddPhysicsObject(new Ball(Ball.BallType.Normal, paddle));

					// manually defining the level layout...
					// but really this should be in a file
					var L = ScreenBoundaries.LeftEdgeOfScreen;
					var R = ScreenBoundaries.RightEdgeOfScreen;
					var T = ScreenBoundaries.TopEdgeOfScreen;
					var B = ScreenBoundaries.BottomEdgeOfScreen;

					int numRows = 3;
					for (int row = 0; row < numRows; row ++)
					{
						// in the top half of the screen, distribute three rows
						var heightDistribute = (T - B) / 2f / 4f;
						var widthDistribute = (R - L) / ((row % 2) + 6f);

						int numCols = (row % 2) + 5;
						for (int col = 0; col < numCols; col++)
						{
							int leftSideSign = ((float) col / (float) numCols) < 0.5f ? -1 : +1;
							physics.AddPhysicsObject(new TargetBox(new Vector2f(L + widthDistribute * (col + 1), 0 + heightDistribute * (row)),
																   (row % 2 == 1) ? 2 : 1));
						}
					}

					// after finished loading, start the game
					ChangeState(State.Game_Play);
					break;

				case State.Game_Play:
					MenuControls.AddInGameUIElements();
					DeltaTime.GetDeltaTime();
					break;

				case State.Game_Pause:
					MenuControls.AddPauseMenuView();
					break;

				case State.Game_End:
					MenuControls.AddGameOverMenuView();
					break;

				case State.Highscores:
					MenuControls.AddHighscoresMenuView();
					break;

				default:
					break;
			}

			stateFinalised = true;
		}

		public void ChangeState(State stateToChangeTo)
		{
			stateFinalised = false;

			switch (state)
			{
				case State.Initialising:
					break;

				case State.Menu_Main:
					MenuControls.RemoveMainMenuView();
					break;

				case State.Menu_About:
					MenuControls.RemoveAboutMenuView();
					break;

				case State.Game_Loading:
					break;

				case State.Game_Play:
					MenuControls.RemoveInGameUIElements();
					if (stateToChangeTo != State.Game_Pause)
					{
						physics.ClearAllPhysicsObjects();
						physics = null;
					}
					scoreTextView = null;
					break;

				case State.Game_Pause:
					MenuControls.RemovePauseMenuView();
					scoreTextView = null;
					break;

				case State.Game_End:
					MenuControls.RemoveGameOverMenuView();
					scoreTextView = null;
					break;

				case State.Highscores:
					MenuControls.RemoveHighscoresView();
					break;

				default:
					break;
			}

			InitialiseState(stateToChangeTo);
		}

		public void PauseStateLoop()
		{
			switch (state)
			{
				case State.Initialising:
					break;

				case State.Menu_Main:
					break;

				case State.Menu_About:
					break;

				case State.Game_Loading:
					ChangeState(State.Game_Pause);
					break;

				case State.Game_Play:
					ChangeState(State.Game_Pause);
					break;

				case State.Game_Pause:
					break;

				case State.Game_End:
					break;

				case State.Highscores:
					break;

				default:
					break;
			}
			stateLoopPaused = true;
		}

		public void ResumeStateLoop()
		{
			if (!stateLoopRunning)
			{
				awaitingStart = true;
				StartStateLoop();
			}
			stateLoopPaused = false;
		}

		public void RunStateLoopUpdate()
		{
			if (awaitingStart)
				StartStateLoop();

			if (stateLoopRunning && !stateLoopPaused && stateFinalised)
			{
				switch (state)
				{
					case State.Initialising:
						break;

					case State.Menu_Main:
						MainMenuStateUpdate();
						break;

					case State.Menu_About:
						AboutMenuStateUpdate();
						break;

					case State.Game_Loading:
						GameLoadingStateUpdate();
						break;

					case State.Game_Play:
						GamePlayStateUpdate();
						break;

					case State.Game_Pause:
						GamePausedStateUpdate();
						break;

					case State.Game_End:
						GameOverStateUpdate();
						break;

					case State.Highscores:
						HighscoresStateUpdate();
						break;

					default:
						break;
				}
			}
		}

		public void MainMenuStateUpdate()
		{
		}

		public void AboutMenuStateUpdate()
		{
		}

		public void GameLoadingStateUpdate()
		{
		}

		internal void SetScoreTextView(TextView scoreText)
		{
			scoreTextView = scoreText;
		}

		internal void UpdateScoreTextView()
		{
			OpenGLES10Activity.GetMainActivity?.RunOnUiThread(() =>
			{
				scoreTextView?.SetText(score.ToString(), TextView.BufferType.Normal);
			});
		}

		public void GamePlayStateUpdate()
		{
			PhysicsUpdate.PhysicsUpdateObject.UpdateAll();
			UpdateScoreTextView();
			// no difference between winning and losing, unfortunately :P
			// but I could make another screen, and have  game_end_loss and game_end_win screens
			if (PhysicsUpdate.PhysicsUpdateObject.noBalls)
				ChangeState(State.Game_End);
			if (PhysicsUpdate.PhysicsUpdateObject.noBoxes)
				ChangeState(State.Game_End);
		}

		public void GamePausedStateUpdate()
		{
			UpdateScoreTextView();
		}

		public void GameOverStateUpdate()
		{
			UpdateScoreTextView();
		}

		public void HighscoresStateUpdate()
		{
		}

		public void StopStateLoop()
		{
			stateLoopRunning = false;
			stateLoopPaused = true;
			awaitingStart = false;
		}

		public void AccelerometerInput(float x, float y, float z)
		{
			if (stateLoopRunning && !stateLoopPaused && stateFinalised)
			{
				switch (state)
				{
					case State.Initialising:
						break;

					case State.Menu_Main:
						break;

					case State.Menu_About:
						break;

					case State.Game_Loading:
						break;

					case State.Game_Play:
						PhysicsUpdate.PhysicsUpdateObject.MovePaddleWithTilt(x);
						break;

					case State.Game_Pause:
						break;

					case State.Game_End:
						break;

					case State.Highscores:
						break;

					default:
						break;
				}
			}
		}

		public void TouchDown()
		{
			if (stateLoopRunning && !stateLoopPaused && stateFinalised)
			{
				switch (state)
				{
					case State.Initialising:
						break;

					case State.Menu_Main:
						break;

					case State.Menu_About:
						break;

					case State.Game_Loading:
						break;

					case State.Game_Play:
						PhysicsUpdate.PhysicsUpdateObject.LaunchAttachedBalls();
						break;

					case State.Game_Pause:
						break;

					case State.Game_End:
						break;

					case State.Highscores:
						break;

					default:
						break;
				}
			}
		}

		void ISurfaceHolderCallback.SurfaceChanged(ISurfaceHolder holder, Format format, int width, int height)
		{
			screenWidth = width;
			screenHeight = height;

			PauseGameAfterSurfaceChange();
			surfaceReady = true;
		}

		void ISurfaceHolderCallback.SurfaceCreated(ISurfaceHolder holder)
		{
			surfaceReady = false;
			ScreenBoundaries.ScreenBoundariesSet = false;
			PauseGameAfterSurfaceChange();
			stateLoopRunning = false;
		}

		void ISurfaceHolderCallback.SurfaceDestroyed(ISurfaceHolder holder)
		{
			surfaceReady = false;
			ScreenBoundaries.ScreenBoundariesSet = false;
			PauseGameAfterSurfaceChange();
			stateLoopRunning = false;
		}

		void IDisposable.Dispose()
		{
		}

		public void PauseGameAfterSurfaceChange()
		{
			switch (state)
			{
				case State.Initialising:
					break;

				case State.Menu_Main:
					break;

				case State.Menu_About:
					break;

				case State.Game_Loading:
					ChangeState(State.Game_Pause);
					break;

				case State.Game_Play:
					ChangeState(State.Game_Pause);
					break;

				case State.Game_Pause:
					break;

				case State.Game_End:
					break;

				case State.Highscores:
					break;

				default:
					break;
			}
		}
	}
}