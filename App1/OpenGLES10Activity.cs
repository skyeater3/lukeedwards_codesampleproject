﻿using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Hardware;
using Android.OS;
using Android.Text;

namespace App1
{
	[Activity(Label = "Lukes OpenGL thing",
		MainLauncher = true,
		Icon = "@drawable/ic_launcher")]
	public class OpenGLES10Activity : Activity, ISensorEventListener, Android.Text.Html.IImageGetter
	{
		private MyGLSurfaceView mGLView;
		private SensorManager mSensorManager;

		private static OpenGLES10Activity mainActivity = null;
		public static OpenGLES10Activity GetMainActivity { get { return mainActivity; } }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			// Create a GLSurfaceView instance and set it
			// as the ContentView for this Activity.
			mGLView = new MyGLSurfaceView(this);
			SetContentView(mGLView);
			mSensorManager = (SensorManager) GetSystemService(Context.SensorService);
			mainActivity = this;
		}

		protected override void OnStart()
		{
			base.OnStart();
			GameStateLoop.GameStateLoopInstance.StartStateLoop();
		}

		protected override void OnResume()
		{
			// The following call resumes a paused rendering thread.
			// If you de-allocated graphic objects for onPause()
			// this is a good place to re-allocate them.
			base.OnResume();
			mGLView.OnResume();
			mSensorManager.RegisterListener(this, mSensorManager.GetDefaultSensor(SensorType.Accelerometer), SensorDelay.Game);

			GameStateLoop.GameStateLoopInstance.ResumeStateLoop();
		}

		protected override void OnPause()
		{
			// The following call pauses the rendering thread.
			// If your OpenGL application is memory intensive,
			// you should consider de-allocating objects that
			// consume significant memory here.
			base.OnPause();
			mGLView.OnPause();
			mSensorManager.UnregisterListener(this);

			GameStateLoop.GameStateLoopInstance.PauseStateLoop();
		}

		protected override void OnStop()
		{
			base.OnStop();

			GameStateLoop.GameStateLoopInstance.StopStateLoop();
		}

		protected override void OnDestroy()
		{
			base.OnDestroy();
		}

		void ISensorEventListener.OnAccuracyChanged(Sensor sensor, SensorStatus accuracy)
		{
		}

		void ISensorEventListener.OnSensorChanged(SensorEvent e)
		{
			if (e.Sensor.Type == SensorType.Accelerometer)
				GameStateLoop.GameStateLoopInstance.AccelerometerInput(e.Values[0], e.Values[1], e.Values[2]);
		}

		Drawable Html.IImageGetter.GetDrawable(string source)
		{
			return null;
		}
	}
}