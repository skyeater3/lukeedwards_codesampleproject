﻿using Android.Content;
using Android.Opengl;
using Android.Views;

namespace App1
{
	/**
	 * A view container where OpenGL ES graphics can be drawn on screen.
	 * This view can also be used to capture touch events, such as a user
	 * interacting with drawn objects.
	 */

	public class MyGLSurfaceView : GLSurfaceView
	{
		private readonly MyGLRenderer mRenderer;

		public MyGLSurfaceView(Context context)
			: base(context)
		{
			// Set the Renderer for drawing on the GLSurfaceView
			mRenderer = new MyGLRenderer();
			SetRenderer(mRenderer);

			// Render the view only when there is a change in the drawing data
			this.RenderMode = Rendermode.Continuously;
			this.Holder.AddCallback(GameStateLoop.GameStateLoopInstance);
		}

		private float mPreviousX;
		private float mPreviousY;
		// private bool ignoreAccelerometerReadings = false;

		public override bool OnTouchEvent(MotionEvent e)
		{
			// MotionEvent reports input details from the touch screen
			// and other input controls. In this case, we are only
			// interested in events where the touch position changed.

			float x = e.GetX();
			float y = e.GetY();

			switch (e.Action)
			{
				case MotionEventActions.Down:
					//ignoreAccelerometerReadings = true;
					GameStateLoop.GameStateLoopInstance.TouchDown();
					break;

				case MotionEventActions.Up:
					//ignoreAccelerometerReadings = false;
					break;
			}

			mPreviousX = x;
			mPreviousY = y;

			return true;
		}

		public override void OnPause()
		{
			base.OnPause();
		}
	}
}