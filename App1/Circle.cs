using Android.Opengl;
using Java.Nio;
using Javax.Microedition.Khronos.Opengles;
using System;

namespace App1
{
	public class Circle
	{
		private readonly FloatBuffer vertexBuffer;
		private readonly ShortBuffer drawListBuffer;

		// number of coordinates per vertex in this array
		private static readonly int COORDS_PER_VERTEX = 3;

		private static float[] circleCoords;
		private static short[] drawOrder;

		private readonly float[] defaultColor = { 1.0f, 0.5f, 0.238039216f, 1.0f };
		private float radius = 1f;

		public void InitialiseCoords(int numSegments)
		{
			double theta = 2 * 3.14159265359 / (double) (numSegments);
			float c = (float) Math.Cos(theta);//precalculate the sine and cosine
			float s = (float) Math.Sin(theta);
			float t;

			float x = radius;//we start at angle = 0
			float y = 0;

			circleCoords = new float[(numSegments + 1) * COORDS_PER_VERTEX];
			circleCoords[0] = circleCoords[1] = circleCoords[2] = 0.0f;     // centre position

			drawOrder = new short[numSegments * COORDS_PER_VERTEX];

			for (int ii = 0; ii < numSegments; ii++)
			{
				circleCoords[(ii * COORDS_PER_VERTEX) + 3] = x;
				circleCoords[(ii * COORDS_PER_VERTEX) + 4] = y;
				circleCoords[(ii * COORDS_PER_VERTEX) + 5] = 0.0f;

				if (ii >= 1)
				{
					drawOrder[0 + ((ii - 1) * COORDS_PER_VERTEX)] = 0;
					drawOrder[1 + ((ii - 1) * COORDS_PER_VERTEX)] = (short) (ii);
					drawOrder[2 + ((ii - 1) * COORDS_PER_VERTEX)] = (short) (ii + 1);
				}
				if (ii == numSegments - 1)
				{
					drawOrder[0 + (ii * COORDS_PER_VERTEX)] = 0;
					drawOrder[1 + (ii * COORDS_PER_VERTEX)] = (short) (ii + 1);
					drawOrder[2 + (ii * COORDS_PER_VERTEX)] = (short) (1);
				}

				//apply the rotation matrix
				t = x;
				x = c * x - s * y;
				y = s * t + c * y;
			}
		}

		public Circle(int numSegments = 12)
		{
			InitialiseCoords(numSegments);

			ByteBuffer bb = ByteBuffer.AllocateDirect(
					// (# of coordinate values * 4 bytes per float)
					circleCoords.Length * 4);
			bb.Order(ByteOrder.NativeOrder());
			vertexBuffer = bb.AsFloatBuffer();
			vertexBuffer.Put(circleCoords);
			vertexBuffer.Position(0);

			// initialize byte buffer for the draw list
			ByteBuffer dlb = ByteBuffer.AllocateDirect(
					// (# of coordinate values * 2 bytes per short)
					drawOrder.Length * 2);
			dlb.Order(ByteOrder.NativeOrder());
			drawListBuffer = dlb.AsShortBuffer();
			drawListBuffer.Put(drawOrder);
			drawListBuffer.Position(0);
		}

		public void Draw(IGL10 gl, float[] color = null)
		{
			if (color == null)
				color = defaultColor;

			// Since this shape uses vertex arrays, enable them
			GLES11.GlEnableClientState(GL11.GlVertexArray);

			// draw the shape
			GLES11.GlColor4f(       // set color
					color[0], color[1],
					color[2], color[3]);
			GLES11.GlVertexPointer( // point to vertex data:
					COORDS_PER_VERTEX,
					GL11.GlFloat, 0, vertexBuffer);
			GLES11.GlDrawElements(  // draw shape:
					GL11.GlTriangles,
					drawOrder.Length, GL11.GlUnsignedShort,
					drawListBuffer);

			// Disable vertex array drawing to avoid
			// conflicts with shapes that don't use it
			GLES11.GlDisableClientState(GL11.GlVertexArray);
		}
	}
}