Visual Studio using Android Xamarin, simple Breakout clone
https://bitbucket.org/skyeater3/lukeedwards_codesampleproject

A sample project, created over about a week, to demonstrate coding and learning capability.

The first app I have created for Android.

Very simple game loop. Using some of the Android API to display UI elements, and menus on the surfaceView I created.


**What works well!**

* Game correctly pauses when interrupted, or activity changes. This was especially useful when testing and people would call me!
* Even if the UI code might be unorthodox it actually works.
* Game runs in good speed on my phone. Not actually sure how this will translate to other devices. It is only very simple, though, so it should be fine.
* The whole game is actually playable! Very pleased that I got it up and running. And in good time.
* Writing it was a great learning experience.


**Known Issues and Unfinished features** (These are all things that can be amended at a later date if I revisit the project)

* Game Over screen is the same regardless of whether the game ends because you destroyed all the blocks, or all balls disappear off the bottom of the screen.
* Physics occasionally seems to skip a few frames when moving paddle intersect with a moving ball on the left/right sides. Didn't have enough time to narrow down exactly what was happening here but I suspect it's to do with the 'eject' code being triggered for a few frames in a row.
* Only one level, which is hard-coded (except the colour of the blocks)
* No in-game documentation of what exactly the power pills do when you collect them.

   For reference: Green = sticky paddle, light blue = large paddle, red = small paddle, gold/orange = 150 points, yellow = speed up balls, dark blue = slow down balls, grey = multi-ball.

   All pills grant 10 points for collecting them (except gold/orange mentioned above). Destroying a block is worth 10 * block hit-points worth of points. 1 point for hitting a ball with the paddle.

* No saved high-scores.
* No visual differences between blocks with more hit-points, or blocks that have been damaged (such as from 2 to 1 hp left)
* Physics is a little too accurate to actually be a lot of fun. Breakout-style games normally have 'trick' physics on the paddle. With the absolute centre allowing proper bounces, but the edges affecting the direction of the ball slightly more. This works for the corners here, but corner collisions are rare/difficult to pull off. If I come back to the project I would like to adjust this.